<?php

declare(strict_types=1);

namespace App\Controller\Api\Open\v1;

use App\Client\Application\ClientService;
use App\Client\Application\CreateClientCommand;
use App\Client\Application\UpdateClientCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ClientController extends AbstractController
{
    private ClientService $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * @Route("/client/{uuid}", methods={"GET"}, name="client_get")
     */
    public function getClient(string $uuid): JsonResponse
    {
        $client = $this->clientService->getById($uuid);

        return new JsonResponse(
            [
                'firstName' => $client->getFirstName(),
                'lastName' => $client->getLastName(),
                'email' => $client->getEmail(),
                'phone' => $client->getPhone(),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/client/{id}", methods={"PUT"}, name="client_update")
     */
    public function updateClient(string $id, Request $request, ValidatorInterface $validator): JsonResponse
    {
        $updateClientCommand = new UpdateClientCommand(
            $request->request->get('firstName'),
            $request->request->get('lastName'),
            $request->request->get('email'),
            $request->request->get('phone')
        );

        $errors = $validator->validate($updateClientCommand);

        if (count($errors) > 0) {
            return new JsonResponse((string) $errors);
        }

        $this->clientService->update($id, $updateClientCommand);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/client", methods={"POST"}, name="client_create")
     */
    public function createClient(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $clientCommand = new CreateClientCommand(
            $request->request->get('firstName'),
            $request->request->get('lastName'),
            $request->request->get('email'),
            $request->request->get('phone')
        );

        $errors = $validator->validate($clientCommand);

        if (count($errors) > 0) {
            return new JsonResponse((string) $errors);
        }

        $this->clientService->create($clientCommand);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/client/{id}", methods={"DELETE"}, name="client_delete")
     */
    public function deleteClient(string $id): void
    {
        $this->clientService->delete($id);
    }
}