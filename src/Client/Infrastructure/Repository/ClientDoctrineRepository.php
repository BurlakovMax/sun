<?php

declare(strict_types=1);

namespace App\Client\Infrastructure\Repository;

use App\Client\Domain\Client;
use App\Client\Domain\ClientStorage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class ClientDoctrineRepository extends ServiceEntityRepository implements ClientStorage
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function getById(string $uuid): ?Client
    {
        return $this->find($uuid);
    }

    public function getAndLock(string $id): ?Client
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    public function create(Client $client): void
    {
        $this->getEntityManager()->persist($client);
    }

    public function delete(Client $client): void
    {
        $this->getEntityManager()->remove($client);
    }
}