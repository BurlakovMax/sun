<?php

declare(strict_types=1);

namespace App\Client\Domain;

use App\Client\Application\CreateClientCommand;
use App\Client\Application\UpdateClientCommand;
use App\Coere\Structures\Email;
use App\Core\Structures\Phone;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

/**
 * @ORM\Entity(repositoryClass="App\Client\Infrastructure\Repository\ClientDoctrineRepository")
 * @ORM\Table(name="client")
 */
class Client
{
    /**
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     */
    private Uuid $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $lastName;

    /**
     * @ORM\Column(type="email", length=255, unique=true)
     */
    private Email $email;

    /**
     * @ORM\Column(type="phone", length=255, unique=true)
     */
    private Phone $phone;

    public function __construct(CreateClientCommand $createClientCommand)
    {
        $this->uuid = new UuidV4();
        $this->firstName = $createClientCommand->getFirstName();
        $this->lastName = $createClientCommand->getLastName();
        $this->email = new Email($createClientCommand->getEmail());
        $this->phone = new Phone($createClientCommand->getPhone());
    }

    public function update(UpdateClientCommand $updateClientCommand)
    {
        $this->firstName = $updateClientCommand->getFirstName() ?? $this->firstName;
        $this->lastName = $updateClientCommand->getLastName() ?? $this->lastName;
        $this->email =
            null !== $updateClientCommand->getEmail() ? new Email($updateClientCommand->getEmail()) : $this->email;
        $this->phone =
            null !== $updateClientCommand->getPhone() ? new Phone($updateClientCommand->getPhone()) : $this->phone;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email->getEmail();
    }

    public function getPhone(): string
    {
        return $this->phone->getPhone();
    }
}