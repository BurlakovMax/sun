<?php

declare(strict_types=1);

namespace App\Client\Domain;

use Symfony\Component\Uid\Uuid;

interface ClientStorage
{
    public function getById(string $uuid): ?Client;

    public function getAndLock(string $id): ?Client;

    public function create(Client $client): void;

    public function delete(Client $client): void;
}