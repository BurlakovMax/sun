<?php

declare(strict_types=1);

namespace App\Client\Application;

use App\Client\Domain\Client;
use Symfony\Component\Uid\Uuid;

final class ClientData
{
    private Uuid $uuid;
    private string $firstName;
    private string $lastName;
    private string $email;
    private string $phone;

    public function __construct(Client $client)
    {
        $this->uuid = $client->getUuid();
        $this->firstName = $client->getFirstName();
        $this->lastName = $client->getLastName();
        $this->email = $client->getEmail();
        $this->phone = $client->getPhone();
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}