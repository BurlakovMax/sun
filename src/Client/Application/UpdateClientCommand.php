<?php

declare(strict_types=1);

namespace App\Client\Application;

use Symfony\Component\Validator\Constraints as Assert;

final class UpdateClientCommand
{
    /**
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-zA-Z]+$/")
     */
    private ?string $firstName;

    /**
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-zA-Z]+$/")
     */
    private ?string $lastName;

    /**
     * @Assert\NotBlank
     * @Assert\Email()
     */
    private ?string $email;

    /**
     * @Assert\NotBlank
     */
    private ?string $phone;

    public function __construct(?string $firstName, ?string $lastName, ?string $email, ?string $phone)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phone = $phone;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }
}