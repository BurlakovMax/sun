<?php

declare(strict_types=1);

namespace App\Client\Application;

use App\Client\Domain\Client;
use App\Client\Domain\ClientStorage;
use App\Core\ApplicationSupport\TransactionManager;

final class ClientService
{
    private ClientStorage $clientStorage;
    private TransactionManager $transactionManager;

    public function __construct(ClientStorage $clientStorage, TransactionManager $transactionManager)
    {
        $this->clientStorage = $clientStorage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function create(CreateClientCommand $createClientCommand): void
    {
        $this->transactionManager->transactional(function () use ($createClientCommand) {
            $this->clientStorage->create(new Client($createClientCommand));
        });
    }

    public function update(string $id, UpdateClientCommand $updateClientCommand): void
    {
        $client = $this->getAndLockClient($id);

        $this->transactionManager->transactional(function () use ($client, $updateClientCommand){
            $client->update($updateClientCommand);
        });
    }

    public function delete(string $id): void
    {
       $this->clientStorage->delete($this->getClient($id));
    }

    public function getById(string $id): ClientData
    {
        return new ClientData($this->getClient($id));
    }

    private function getClient(string $id): Client
    {
        $client = $this->clientStorage->getById($id);

        if (null === $client) {
            throw new ClientNotFound();
        }

        return $client;
    }

    private function getAndLockClient(string $id): Client
    {
        $client = $this->clientStorage->getAndLock($id);

        if (null === $client) {
            throw new ClientNotFound();
        }

        return $client;
    }
}