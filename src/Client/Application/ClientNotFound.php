<?php

declare(strict_types=1);

namespace App\Client\Application;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class ClientNotFound extends NotFoundHttpException
{

}