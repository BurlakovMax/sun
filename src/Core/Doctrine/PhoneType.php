<?php

declare(strict_types=1);

namespace App\Core\Doctrine;

use App\Core\Structures\Phone;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class PhoneType extends StringType
{
    public function getName(): string
    {
        return 'phone';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Phone
    {
        return new Phone($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof Phone) {
            throw new \InvalidArgumentException('Value must be ' . Phone::class . ' type');
        }

        return $value;
    }
}