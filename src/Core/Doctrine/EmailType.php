<?php

declare(strict_types=1);

namespace App\Core\Doctrine;

use App\Coere\Structures\Email;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class EmailType extends StringType
{
    public function getName(): string
    {
        return 'email';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Email
    {
        return new Email($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof Email) {
            throw new \InvalidArgumentException('Value must be ' . Email::class . ' type');
        }

        return $value;
    }
}