<?php

declare(strict_types=1);

namespace App\Coere\Structures;

use http\Exception\InvalidArgumentException;

final class Email
{
    private string $email;

    public function __construct(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Not valid email address');
        }

        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}